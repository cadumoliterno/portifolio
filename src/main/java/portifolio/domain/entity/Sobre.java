package portifolio.domain.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.validation.constraints.NotNull;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Sobre {
 

@JsonIgnore
@ManyToOne
@JoinColumn(name = "id_usuario")
private Usuario id_usuario;

@Column(name = "descricao", length = 0)
private String descricao;

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id_sobre")
private Integer id_sobre;

@JsonIgnore
@ManyToOne
@JoinColumn(name = "id_endereco")
private Endereco id_endereco;

@JsonIgnore
@ManyToOne
@JoinColumn(name = "id_contato")
private Contato id_contato;


 
}
