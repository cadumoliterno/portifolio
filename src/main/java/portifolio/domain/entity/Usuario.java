package portifolio.domain.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.validation.constraints.NotNull;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Usuario {
 

@Column(name = "login", length = 0)
private String login;

@Column(name = "senha", length = 0)
private String senha;

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id_usuario")
private Integer id_usuario;

@Column(name = "nome", length = 0)
private String nome;


@JsonIgnore
@OneToMany(mappedBy = "id_sobre", fetch = FetchType.LAZY )
Set<Sobre> sobre =  new HashSet<Sobre>();

@JsonIgnore
@OneToMany(mappedBy = "id_formacao", fetch = FetchType.LAZY )
Set<Formacao> formacao =  new HashSet<Formacao>();

@JsonIgnore
@OneToMany(mappedBy = "id_experiencia", fetch = FetchType.LAZY )
Set<Experiencia> experiencia =  new HashSet<Experiencia>();

@JsonIgnore
@OneToMany(mappedBy = "id_documento", fetch = FetchType.LAZY )
Set<Documento> documento =  new HashSet<Documento>();

@JsonIgnore
@OneToMany(mappedBy = "id_habilidade", fetch = FetchType.LAZY )
Set<Habilidade> habilidade =  new HashSet<Habilidade>();

 
}
