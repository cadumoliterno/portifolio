package portifolio.domain.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.validation.constraints.NotNull;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Endereco {
 

@Column(name = "uf", length = 10)
private String uf;

@Column(name = "bairro", length = 0)
private String bairro;

@Column(name = "logradouro", length = 0)
private String logradouro;

@Column(name = "cidade", length = 0)
private String cidade;

@Column(name = "complemento", length = 0)
private String complemento;

@Column(name = "rua", length = 0)
private String rua;

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id_endereco")
private Integer id_endereco;


@JsonIgnore
@OneToMany(mappedBy = "id_sobre", fetch = FetchType.LAZY )
Set<Sobre> sobre =  new HashSet<Sobre>();

 
}
