package portifolio.domain.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.validation.constraints.NotNull;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Contato {
 

@Column(name = "telefone_2", length = 50)
private String telefone_2;

@Column(name = "email", length = 0)
private String email;

@Column(name = "celular_2", length = 50)
private String celular_2;

@Column(name = "celular_1", length = 50)
private String celular_1;

@Column(name = "telefone_1", length = 50)
private String telefone_1;

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id_contato")
private Integer id_contato;


@JsonIgnore
@OneToMany(mappedBy = "id_sobre", fetch = FetchType.LAZY )
Set<Sobre> sobre =  new HashSet<Sobre>();

 
}
