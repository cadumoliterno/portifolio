package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/documento" )
public class DocumentoController {
 
private DocumentoRepository documento;

    @Autowired
    public DocumentoController(DocumentoRepository documento){
        this.documento = documento;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Documento save ( @RequestBody @Valid Documento documento ){
        return this.documento.save(documento);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  SelecionarTodosDocumento (){
        return this.documento.SelecionarTodosDocumento();
    }

    @GetMapping("/EncontrarPorLikeTipo")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorLikeTipo ( @RequestParam @Valid  BigDecimal tipo ){
        return this.documento.EncontrarPorLikeTipo(tipo);
    }

    @GetMapping("/EncontrarPorLikeImagem")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorLikeImagem ( @RequestParam @Valid  Byte[] imagem ){
        return this.documento.EncontrarPorLikeImagem(imagem);
    }

    @GetMapping("/EncontrarPorLikeObservacao")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorLikeObservacao ( @RequestParam @Valid  String observacao ){
        return this.documento.EncontrarPorLikeObservacao(observacao);
    }

    @GetMapping("/EncontrarPorTipo")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorTipo ( @RequestParam @Valid  BigDecimal tipo ){
        return this.documento.EncontrarPorTipo(tipo);
    }

    @GetMapping("/EncontrarPorImagem")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorImagem ( @RequestParam @Valid  Byte[] imagem ){
        return this.documento.EncontrarPorImagem(imagem);
    }

    @GetMapping("/EncontrarPorObservacao")
    @ResponseStatus(HttpStatus.OK)
    public List<Documento>  EncontrarPorObservacao ( @RequestParam @Valid  String observacao ){
        return this.documento.EncontrarPorObservacao(observacao);
    }

    @DeleteMapping("/DeletarPorId_documento")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_documento ( @RequestParam @Valid  Integer id_documento ){
        return this.documento.DeletarPorId_documento(id_documento);
    }

    @DeleteMapping("/DeletarPorTipo")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorTipo ( @RequestParam @Valid  BigDecimal tipo ){
        return this.documento.DeletarPorTipo(tipo);
    }

    @DeleteMapping("/DeletarPorImagem")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorImagem ( @RequestParam @Valid  Byte[] imagem ){
        return this.documento.DeletarPorImagem(imagem);
    }

    @DeleteMapping("/DeletarPorObservacao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorObservacao ( @RequestParam @Valid  String observacao ){
        return this.documento.DeletarPorObservacao(observacao);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_usuarioTipo")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_usuarioTipo ( @RequestParam @Valid  BigDecimal tipo ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_usuarioTipo(tipo);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_usuarioImagem")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_usuarioImagem ( @RequestParam @Valid  Byte[] imagem ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_usuarioImagem(imagem);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_usuarioObservacao")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_usuarioObservacao ( @RequestParam @Valid  String observacao ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_usuarioObservacao(observacao);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_certificadoTipo")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_certificadoTipo ( @RequestParam @Valid  BigDecimal tipo ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_certificadoTipo(tipo);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_certificadoImagem")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_certificadoImagem ( @RequestParam @Valid  Byte[] imagem ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_certificadoImagem(imagem);
    }

    @GetMapping("/ProcurarPorDocumentoJoinFetchId_certificadoObservacao")
    @ResponseStatus(HttpStatus.OK)
    public Documento ProcurarPorDocumentoJoinFetchId_certificadoObservacao ( @RequestParam @Valid  String observacao ){
        return this.documento.ProcurarPorDocumentoJoinFetchId_certificadoObservacao(observacao);
    }

 
}
