package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/endereco" )
public class EnderecoController {
 
private EnderecoRepository endereco;

private SobreRepository sobre;

    @Autowired
    public EnderecoController(SobreRepository sobre, EnderecoRepository endereco){
        this.sobre = sobre;
        this.endereco = endereco;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Endereco save ( @RequestBody @Valid Endereco endereco ){
        return this.endereco.save(endereco);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  SelecionarTodosEndereco (){
        return this.endereco.SelecionarTodosEndereco();
    }

    @GetMapping("/EncontrarPorLikeUf")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeUf ( @RequestParam @Valid  String uf ){
        return this.endereco.EncontrarPorLikeUf(uf);
    }

    @GetMapping("/EncontrarPorLikeBairro")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeBairro ( @RequestParam @Valid  String bairro ){
        return this.endereco.EncontrarPorLikeBairro(bairro);
    }

    @GetMapping("/EncontrarPorLikeLogradouro")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeLogradouro ( @RequestParam @Valid  String logradouro ){
        return this.endereco.EncontrarPorLikeLogradouro(logradouro);
    }

    @GetMapping("/EncontrarPorLikeCidade")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeCidade ( @RequestParam @Valid  String cidade ){
        return this.endereco.EncontrarPorLikeCidade(cidade);
    }

    @GetMapping("/EncontrarPorLikeComplemento")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeComplemento ( @RequestParam @Valid  String complemento ){
        return this.endereco.EncontrarPorLikeComplemento(complemento);
    }

    @GetMapping("/EncontrarPorLikeRua")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLikeRua ( @RequestParam @Valid  String rua ){
        return this.endereco.EncontrarPorLikeRua(rua);
    }

    @GetMapping("/EncontrarPorUf")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorUf ( @RequestParam @Valid  String uf ){
        return this.endereco.EncontrarPorUf(uf);
    }

    @GetMapping("/EncontrarPorBairro")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorBairro ( @RequestParam @Valid  String bairro ){
        return this.endereco.EncontrarPorBairro(bairro);
    }

    @GetMapping("/EncontrarPorLogradouro")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorLogradouro ( @RequestParam @Valid  String logradouro ){
        return this.endereco.EncontrarPorLogradouro(logradouro);
    }

    @GetMapping("/EncontrarPorCidade")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorCidade ( @RequestParam @Valid  String cidade ){
        return this.endereco.EncontrarPorCidade(cidade);
    }

    @GetMapping("/EncontrarPorComplemento")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorComplemento ( @RequestParam @Valid  String complemento ){
        return this.endereco.EncontrarPorComplemento(complemento);
    }

    @GetMapping("/EncontrarPorRua")
    @ResponseStatus(HttpStatus.OK)
    public List<Endereco>  EncontrarPorRua ( @RequestParam @Valid  String rua ){
        return this.endereco.EncontrarPorRua(rua);
    }

    @DeleteMapping("/DeletarPorUf")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorUf ( @RequestParam @Valid  String uf ){
        return this.endereco.DeletarPorUf(uf);
    }

    @DeleteMapping("/DeletarPorBairro")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorBairro ( @RequestParam @Valid  String bairro ){
        return this.endereco.DeletarPorBairro(bairro);
    }

    @DeleteMapping("/DeletarPorLogradouro")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorLogradouro ( @RequestParam @Valid  String logradouro ){
        return this.endereco.DeletarPorLogradouro(logradouro);
    }

    @DeleteMapping("/DeletarPorCidade")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorCidade ( @RequestParam @Valid  String cidade ){
        return this.endereco.DeletarPorCidade(cidade);
    }

    @DeleteMapping("/DeletarPorComplemento")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorComplemento ( @RequestParam @Valid  String complemento ){
        return this.endereco.DeletarPorComplemento(complemento);
    }

    @DeleteMapping("/DeletarPorRua")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorRua ( @RequestParam @Valid  String rua ){
        return this.endereco.DeletarPorRua(rua);
    }

    @DeleteMapping("/DeletarPorId_endereco")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_endereco ( @RequestParam @Valid  Integer id_endereco ){
        return this.endereco.DeletarPorId_endereco(id_endereco);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreUf")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreUf ( @RequestParam @Valid  String uf ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreUf(uf);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreBairro")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreBairro ( @RequestParam @Valid  String bairro ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreBairro(bairro);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreLogradouro")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreLogradouro ( @RequestParam @Valid  String logradouro ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreLogradouro(logradouro);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreCidade")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreCidade ( @RequestParam @Valid  String cidade ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreCidade(cidade);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreComplemento")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreComplemento ( @RequestParam @Valid  String complemento ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreComplemento(complemento);
    }

    @GetMapping("/ProcurarPorEnderecoLeftJoinFetchSobreRua")
    @ResponseStatus(HttpStatus.OK)
    public Endereco ProcurarPorEnderecoLeftJoinFetchSobreRua ( @RequestParam @Valid  String rua ){
        return this.endereco.ProcurarPorEnderecoLeftJoinFetchSobreRua(rua);
    }

 
}
