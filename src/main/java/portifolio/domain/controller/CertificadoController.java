package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/certificado" )
public class CertificadoController {
 
private CertificadoRepository certificado;

private DocumentoRepository documento;

    @Autowired
    public CertificadoController(DocumentoRepository documento, CertificadoRepository certificado){
        this.documento = documento;
        this.certificado = certificado;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Certificado save ( @RequestBody @Valid Certificado certificado ){
        return this.certificado.save(certificado);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Certificado>  SelecionarTodosCertificado (){
        return this.certificado.SelecionarTodosCertificado();
    }

    @GetMapping("/EncontrarPorLikeDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Certificado>  EncontrarPorLikeDescricao ( @RequestParam @Valid  String descricao ){
        return this.certificado.EncontrarPorLikeDescricao(descricao);
    }

    @GetMapping("/EncontrarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Certificado>  EncontrarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.certificado.EncontrarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.certificado.DeletarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorId_certificado")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_certificado ( @RequestParam @Valid  Integer id_certificado ){
        return this.certificado.DeletarPorId_certificado(id_certificado);
    }

    @GetMapping("/ProcurarPorCertificadoLeftJoinFetchDocumentoDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Certificado ProcurarPorCertificadoLeftJoinFetchDocumentoDescricao ( @RequestParam @Valid  String descricao ){
        return this.certificado.ProcurarPorCertificadoLeftJoinFetchDocumentoDescricao(descricao);
    }

 
}
