package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/contato" )
public class ContatoController {
 
private ContatoRepository contato;

private SobreRepository sobre;

    @Autowired
    public ContatoController(SobreRepository sobre, ContatoRepository contato){
        this.sobre = sobre;
        this.contato = contato;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato save ( @RequestBody @Valid Contato contato ){
        return this.contato.save(contato);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  SelecionarTodosContato (){
        return this.contato.SelecionarTodosContato();
    }

    @GetMapping("/EncontrarPorLikeTelefone_2")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorLikeTelefone_2 ( @RequestParam @Valid  String telefone_2 ){
        return this.contato.EncontrarPorLikeTelefone_2(telefone_2);
    }

    @GetMapping("/EncontrarPorLikeEmail")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorLikeEmail ( @RequestParam @Valid  String email ){
        return this.contato.EncontrarPorLikeEmail(email);
    }

    @GetMapping("/EncontrarPorLikeCelular_2")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorLikeCelular_2 ( @RequestParam @Valid  String celular_2 ){
        return this.contato.EncontrarPorLikeCelular_2(celular_2);
    }

    @GetMapping("/EncontrarPorLikeCelular_1")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorLikeCelular_1 ( @RequestParam @Valid  String celular_1 ){
        return this.contato.EncontrarPorLikeCelular_1(celular_1);
    }

    @GetMapping("/EncontrarPorLikeTelefone_1")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorLikeTelefone_1 ( @RequestParam @Valid  String telefone_1 ){
        return this.contato.EncontrarPorLikeTelefone_1(telefone_1);
    }

    @GetMapping("/EncontrarPorTelefone_2")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorTelefone_2 ( @RequestParam @Valid  String telefone_2 ){
        return this.contato.EncontrarPorTelefone_2(telefone_2);
    }

    @GetMapping("/EncontrarPorEmail")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorEmail ( @RequestParam @Valid  String email ){
        return this.contato.EncontrarPorEmail(email);
    }

    @GetMapping("/EncontrarPorCelular_2")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorCelular_2 ( @RequestParam @Valid  String celular_2 ){
        return this.contato.EncontrarPorCelular_2(celular_2);
    }

    @GetMapping("/EncontrarPorCelular_1")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorCelular_1 ( @RequestParam @Valid  String celular_1 ){
        return this.contato.EncontrarPorCelular_1(celular_1);
    }

    @GetMapping("/EncontrarPorTelefone_1")
    @ResponseStatus(HttpStatus.OK)
    public List<Contato>  EncontrarPorTelefone_1 ( @RequestParam @Valid  String telefone_1 ){
        return this.contato.EncontrarPorTelefone_1(telefone_1);
    }

    @DeleteMapping("/DeletarPorTelefone_2")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorTelefone_2 ( @RequestParam @Valid  String telefone_2 ){
        return this.contato.DeletarPorTelefone_2(telefone_2);
    }

    @DeleteMapping("/DeletarPorEmail")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorEmail ( @RequestParam @Valid  String email ){
        return this.contato.DeletarPorEmail(email);
    }

    @DeleteMapping("/DeletarPorCelular_2")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorCelular_2 ( @RequestParam @Valid  String celular_2 ){
        return this.contato.DeletarPorCelular_2(celular_2);
    }

    @DeleteMapping("/DeletarPorCelular_1")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorCelular_1 ( @RequestParam @Valid  String celular_1 ){
        return this.contato.DeletarPorCelular_1(celular_1);
    }

    @DeleteMapping("/DeletarPorTelefone_1")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorTelefone_1 ( @RequestParam @Valid  String telefone_1 ){
        return this.contato.DeletarPorTelefone_1(telefone_1);
    }

    @DeleteMapping("/DeletarPorId_contato")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_contato ( @RequestParam @Valid  Integer id_contato ){
        return this.contato.DeletarPorId_contato(id_contato);
    }

    @GetMapping("/ProcurarPorContatoLeftJoinFetchSobreTelefone_2")
    @ResponseStatus(HttpStatus.OK)
    public Contato ProcurarPorContatoLeftJoinFetchSobreTelefone_2 ( @RequestParam @Valid  String telefone_2 ){
        return this.contato.ProcurarPorContatoLeftJoinFetchSobreTelefone_2(telefone_2);
    }

    @GetMapping("/ProcurarPorContatoLeftJoinFetchSobreEmail")
    @ResponseStatus(HttpStatus.OK)
    public Contato ProcurarPorContatoLeftJoinFetchSobreEmail ( @RequestParam @Valid  String email ){
        return this.contato.ProcurarPorContatoLeftJoinFetchSobreEmail(email);
    }

    @GetMapping("/ProcurarPorContatoLeftJoinFetchSobreCelular_2")
    @ResponseStatus(HttpStatus.OK)
    public Contato ProcurarPorContatoLeftJoinFetchSobreCelular_2 ( @RequestParam @Valid  String celular_2 ){
        return this.contato.ProcurarPorContatoLeftJoinFetchSobreCelular_2(celular_2);
    }

    @GetMapping("/ProcurarPorContatoLeftJoinFetchSobreCelular_1")
    @ResponseStatus(HttpStatus.OK)
    public Contato ProcurarPorContatoLeftJoinFetchSobreCelular_1 ( @RequestParam @Valid  String celular_1 ){
        return this.contato.ProcurarPorContatoLeftJoinFetchSobreCelular_1(celular_1);
    }

    @GetMapping("/ProcurarPorContatoLeftJoinFetchSobreTelefone_1")
    @ResponseStatus(HttpStatus.OK)
    public Contato ProcurarPorContatoLeftJoinFetchSobreTelefone_1 ( @RequestParam @Valid  String telefone_1 ){
        return this.contato.ProcurarPorContatoLeftJoinFetchSobreTelefone_1(telefone_1);
    }

 
}
