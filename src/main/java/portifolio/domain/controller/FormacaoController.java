package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/formacao" )
public class FormacaoController {
 
private FormacaoRepository formacao;

    @Autowired
    public FormacaoController(FormacaoRepository formacao){
        this.formacao = formacao;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Formacao save ( @RequestBody @Valid Formacao formacao ){
        return this.formacao.save(formacao);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Formacao>  SelecionarTodosFormacao (){
        return this.formacao.SelecionarTodosFormacao();
    }

    @GetMapping("/EncontrarPorLikeDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Formacao>  EncontrarPorLikeDescricao ( @RequestParam @Valid  String descricao ){
        return this.formacao.EncontrarPorLikeDescricao(descricao);
    }

    @GetMapping("/EncontrarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Formacao>  EncontrarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.formacao.EncontrarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorId_formacao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_formacao ( @RequestParam @Valid  Integer id_formacao ){
        return this.formacao.DeletarPorId_formacao(id_formacao);
    }

    @DeleteMapping("/DeletarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.formacao.DeletarPorDescricao(descricao);
    }

    @GetMapping("/ProcurarPorFormacaoJoinFetchId_usuarioDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Formacao ProcurarPorFormacaoJoinFetchId_usuarioDescricao ( @RequestParam @Valid  String descricao ){
        return this.formacao.ProcurarPorFormacaoJoinFetchId_usuarioDescricao(descricao);
    }

 
}
