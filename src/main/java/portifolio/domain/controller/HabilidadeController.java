package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/habilidade" )
public class HabilidadeController {
 
private HabilidadeRepository habilidade;

    @Autowired
    public HabilidadeController(HabilidadeRepository habilidade){
        this.habilidade = habilidade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Habilidade save ( @RequestBody @Valid Habilidade habilidade ){
        return this.habilidade.save(habilidade);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Habilidade>  SelecionarTodosHabilidade (){
        return this.habilidade.SelecionarTodosHabilidade();
    }

    @GetMapping("/EncontrarPorLikeDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Habilidade>  EncontrarPorLikeDescricao ( @RequestParam @Valid  String descricao ){
        return this.habilidade.EncontrarPorLikeDescricao(descricao);
    }

    @GetMapping("/EncontrarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Habilidade>  EncontrarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.habilidade.EncontrarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.habilidade.DeletarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorId_habilidade")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_habilidade ( @RequestParam @Valid  Integer id_habilidade ){
        return this.habilidade.DeletarPorId_habilidade(id_habilidade);
    }

    @GetMapping("/ProcurarPorHabilidadeJoinFetchId_usuarioDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Habilidade ProcurarPorHabilidadeJoinFetchId_usuarioDescricao ( @RequestParam @Valid  String descricao ){
        return this.habilidade.ProcurarPorHabilidadeJoinFetchId_usuarioDescricao(descricao);
    }

 
}
