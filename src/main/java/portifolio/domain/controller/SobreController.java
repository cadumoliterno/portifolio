package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/sobre" )
public class SobreController {
 
private SobreRepository sobre;

    @Autowired
    public SobreController(SobreRepository sobre){
        this.sobre = sobre;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Sobre save ( @RequestBody @Valid Sobre sobre ){
        return this.sobre.save(sobre);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Sobre>  SelecionarTodosSobre (){
        return this.sobre.SelecionarTodosSobre();
    }

    @GetMapping("/EncontrarPorLikeDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Sobre>  EncontrarPorLikeDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.EncontrarPorLikeDescricao(descricao);
    }

    @GetMapping("/EncontrarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Sobre>  EncontrarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.EncontrarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.DeletarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorId_sobre")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_sobre ( @RequestParam @Valid  Integer id_sobre ){
        return this.sobre.DeletarPorId_sobre(id_sobre);
    }

    @GetMapping("/ProcurarPorSobreJoinFetchId_usuarioDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Sobre ProcurarPorSobreJoinFetchId_usuarioDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.ProcurarPorSobreJoinFetchId_usuarioDescricao(descricao);
    }

    @GetMapping("/ProcurarPorSobreJoinFetchId_contatoDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Sobre ProcurarPorSobreJoinFetchId_contatoDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.ProcurarPorSobreJoinFetchId_contatoDescricao(descricao);
    }

    @GetMapping("/ProcurarPorSobreJoinFetchId_enderecoDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Sobre ProcurarPorSobreJoinFetchId_enderecoDescricao ( @RequestParam @Valid  String descricao ){
        return this.sobre.ProcurarPorSobreJoinFetchId_enderecoDescricao(descricao);
    }

 
}
