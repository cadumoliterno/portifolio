package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/usuario" )
public class UsuarioController {
 
private UsuarioRepository usuario;

private SobreRepository sobre;

private FormacaoRepository formacao;

private ExperienciaRepository experiencia;

private DocumentoRepository documento;

private HabilidadeRepository habilidade;

    @Autowired
    public UsuarioController(SobreRepository sobre, FormacaoRepository formacao, ExperienciaRepository experiencia, DocumentoRepository documento, HabilidadeRepository habilidade, UsuarioRepository usuario){
        this.sobre = sobre;
        this.formacao = formacao;
        this.experiencia = experiencia;
        this.documento = documento;
        this.habilidade = habilidade;
        this.usuario = usuario;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario save ( @RequestBody @Valid Usuario usuario ){
        return this.usuario.save(usuario);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  SelecionarTodosUsuario (){
        return this.usuario.SelecionarTodosUsuario();
    }

    @GetMapping("/EncontrarPorLikeLogin")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorLikeLogin ( @RequestParam @Valid  String login ){
        return this.usuario.EncontrarPorLikeLogin(login);
    }

    @GetMapping("/EncontrarPorLikeSenha")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorLikeSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.EncontrarPorLikeSenha(senha);
    }

    @GetMapping("/EncontrarPorLikeNome")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorLikeNome ( @RequestParam @Valid  String nome ){
        return this.usuario.EncontrarPorLikeNome(nome);
    }

    @GetMapping("/EncontrarPorLogin")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorLogin ( @RequestParam @Valid  String login ){
        return this.usuario.EncontrarPorLogin(login);
    }

    @GetMapping("/EncontrarPorSenha")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.EncontrarPorSenha(senha);
    }

    @GetMapping("/EncontrarPorNome")
    @ResponseStatus(HttpStatus.OK)
    public List<Usuario>  EncontrarPorNome ( @RequestParam @Valid  String nome ){
        return this.usuario.EncontrarPorNome(nome);
    }

    @DeleteMapping("/DeletarPorLogin")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorLogin ( @RequestParam @Valid  String login ){
        return this.usuario.DeletarPorLogin(login);
    }

    @DeleteMapping("/DeletarPorSenha")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.DeletarPorSenha(senha);
    }

    @DeleteMapping("/DeletarPorId_usuario")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_usuario ( @RequestParam @Valid  Integer id_usuario ){
        return this.usuario.DeletarPorId_usuario(id_usuario);
    }

    @DeleteMapping("/DeletarPorNome")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorNome ( @RequestParam @Valid  String nome ){
        return this.usuario.DeletarPorNome(nome);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchSobreLogin")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchSobreLogin ( @RequestParam @Valid  String login ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchSobreLogin(login);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchSobreSenha")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchSobreSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchSobreSenha(senha);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchSobreNome")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchSobreNome ( @RequestParam @Valid  String nome ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchSobreNome(nome);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchFormacaoLogin")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoLogin ( @RequestParam @Valid  String login ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchFormacaoLogin(login);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchFormacaoSenha")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchFormacaoSenha(senha);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchFormacaoNome")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoNome ( @RequestParam @Valid  String nome ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchFormacaoNome(nome);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchExperienciaLogin")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaLogin ( @RequestParam @Valid  String login ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchExperienciaLogin(login);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchExperienciaSenha")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchExperienciaSenha(senha);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchExperienciaNome")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaNome ( @RequestParam @Valid  String nome ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchExperienciaNome(nome);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchDocumentoLogin")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoLogin ( @RequestParam @Valid  String login ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchDocumentoLogin(login);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchDocumentoSenha")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchDocumentoSenha(senha);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchDocumentoNome")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoNome ( @RequestParam @Valid  String nome ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchDocumentoNome(nome);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchHabilidadeLogin")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeLogin ( @RequestParam @Valid  String login ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchHabilidadeLogin(login);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchHabilidadeSenha")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeSenha ( @RequestParam @Valid  String senha ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchHabilidadeSenha(senha);
    }

    @GetMapping("/ProcurarPorUsuarioLeftJoinFetchHabilidadeNome")
    @ResponseStatus(HttpStatus.OK)
    public Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeNome ( @RequestParam @Valid  String nome ){
        return this.usuario.ProcurarPorUsuarioLeftJoinFetchHabilidadeNome(nome);
    }

 
}
