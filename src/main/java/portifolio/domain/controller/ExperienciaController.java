package portifolio.domain.controller;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import java.util.function.Supplier;
import portifolio.domain.repository.*;
import portifolio.domain.entity.*;

@RestController
@RequestMapping(value = "/api/experiencia" )
public class ExperienciaController {
 
private ExperienciaRepository experiencia;

    @Autowired
    public ExperienciaController(ExperienciaRepository experiencia){
        this.experiencia = experiencia;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Experiencia save ( @RequestBody @Valid Experiencia experiencia ){
        return this.experiencia.save(experiencia);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Experiencia>  SelecionarTodosExperiencia (){
        return this.experiencia.SelecionarTodosExperiencia();
    }

    @GetMapping("/EncontrarPorLikeDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Experiencia>  EncontrarPorLikeDescricao ( @RequestParam @Valid  String descricao ){
        return this.experiencia.EncontrarPorLikeDescricao(descricao);
    }

    @GetMapping("/EncontrarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public List<Experiencia>  EncontrarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.experiencia.EncontrarPorDescricao(descricao);
    }

    @DeleteMapping("/DeletarPorId_experiencia")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorId_experiencia ( @RequestParam @Valid  Integer id_experiencia ){
        return this.experiencia.DeletarPorId_experiencia(id_experiencia);
    }

    @DeleteMapping("/DeletarPorDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Integer DeletarPorDescricao ( @RequestParam @Valid  String descricao ){
        return this.experiencia.DeletarPorDescricao(descricao);
    }

    @GetMapping("/ProcurarPorExperienciaJoinFetchId_usuarioDescricao")
    @ResponseStatus(HttpStatus.OK)
    public Experiencia ProcurarPorExperienciaJoinFetchId_usuarioDescricao ( @RequestParam @Valid  String descricao ){
        return this.experiencia.ProcurarPorExperienciaJoinFetchId_usuarioDescricao(descricao);
    }

 
}
