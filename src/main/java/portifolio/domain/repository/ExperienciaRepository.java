package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface ExperienciaRepository extends JpaRepository<Experiencia, Integer>{
 
@Query(value = " Select * From experiencia Where descricao = :descricao " ,nativeQuery = true)
List<Experiencia> EncontrarPorDescricao (@Param("descricao") String descricao);

@Query(value = " Select e From Experiencia e ")
List<Experiencia> SelecionarTodosExperiencia();

@Modifying
@Transactional
@Query(value = " Delete From Experiencia Where id_experiencia =:id_experiencia")
Integer DeletarPorId_experiencia (Integer id_experiencia);

@Modifying
@Transactional
@Query(value = " Delete From Experiencia Where descricao =:descricao")
Integer DeletarPorDescricao (String descricao);

@Query(value = " Select e From Experiencia e Join Fetch e.id_usuario Where e.descricao =:descricao ")
Experiencia ProcurarPorExperienciaJoinFetchId_usuarioDescricao(@Param("descricao") String descricao);

@Query(value = " Select * From experiencia Where descricao Like CONCAT('%',:descricao,'%') " ,nativeQuery = true)
List<Experiencia> EncontrarPorLikeDescricao (@Param("descricao") String descricao);


 
}
