package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
 
@Query(value = " Select * From usuario Where login = :login " ,nativeQuery = true)
List<Usuario> EncontrarPorLogin (@Param("login") String login);

@Query(value = " Select * From usuario Where senha = :senha " ,nativeQuery = true)
List<Usuario> EncontrarPorSenha (@Param("senha") String senha);

@Query(value = " Select * From usuario Where nome = :nome " ,nativeQuery = true)
List<Usuario> EncontrarPorNome (@Param("nome") String nome);

@Query(value = " Select u From Usuario u ")
List<Usuario> SelecionarTodosUsuario();

@Modifying
@Transactional
@Query(value = " Delete From Usuario Where login =:login")
Integer DeletarPorLogin (String login);

@Modifying
@Transactional
@Query(value = " Delete From Usuario Where senha =:senha")
Integer DeletarPorSenha (String senha);

@Modifying
@Transactional
@Query(value = " Delete From Usuario Where id_usuario =:id_usuario")
Integer DeletarPorId_usuario (Integer id_usuario);

@Modifying
@Transactional
@Query(value = " Delete From Usuario Where nome =:nome")
Integer DeletarPorNome (String nome);

@Query(value = " Select u From Usuario u Left join Fetch u.sobre Where u.login =:login ")
Usuario ProcurarPorUsuarioLeftJoinFetchSobreLogin(@Param("login") String login);

@Query(value = " Select u From Usuario u Left join Fetch u.sobre Where u.senha =:senha ")
Usuario ProcurarPorUsuarioLeftJoinFetchSobreSenha(@Param("senha") String senha);

@Query(value = " Select u From Usuario u Left join Fetch u.sobre Where u.nome =:nome ")
Usuario ProcurarPorUsuarioLeftJoinFetchSobreNome(@Param("nome") String nome);

@Query(value = " Select u From Usuario u Left join Fetch u.formacao Where u.login =:login ")
Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoLogin(@Param("login") String login);

@Query(value = " Select u From Usuario u Left join Fetch u.formacao Where u.senha =:senha ")
Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoSenha(@Param("senha") String senha);

@Query(value = " Select u From Usuario u Left join Fetch u.formacao Where u.nome =:nome ")
Usuario ProcurarPorUsuarioLeftJoinFetchFormacaoNome(@Param("nome") String nome);

@Query(value = " Select u From Usuario u Left join Fetch u.experiencia Where u.login =:login ")
Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaLogin(@Param("login") String login);

@Query(value = " Select u From Usuario u Left join Fetch u.experiencia Where u.senha =:senha ")
Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaSenha(@Param("senha") String senha);

@Query(value = " Select u From Usuario u Left join Fetch u.experiencia Where u.nome =:nome ")
Usuario ProcurarPorUsuarioLeftJoinFetchExperienciaNome(@Param("nome") String nome);

@Query(value = " Select u From Usuario u Left join Fetch u.documento Where u.login =:login ")
Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoLogin(@Param("login") String login);

@Query(value = " Select u From Usuario u Left join Fetch u.documento Where u.senha =:senha ")
Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoSenha(@Param("senha") String senha);

@Query(value = " Select u From Usuario u Left join Fetch u.documento Where u.nome =:nome ")
Usuario ProcurarPorUsuarioLeftJoinFetchDocumentoNome(@Param("nome") String nome);

@Query(value = " Select u From Usuario u Left join Fetch u.habilidade Where u.login =:login ")
Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeLogin(@Param("login") String login);

@Query(value = " Select u From Usuario u Left join Fetch u.habilidade Where u.senha =:senha ")
Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeSenha(@Param("senha") String senha);

@Query(value = " Select u From Usuario u Left join Fetch u.habilidade Where u.nome =:nome ")
Usuario ProcurarPorUsuarioLeftJoinFetchHabilidadeNome(@Param("nome") String nome);

@Query(value = " Select * From usuario Where login Like CONCAT('%',:login,'%') " ,nativeQuery = true)
List<Usuario> EncontrarPorLikeLogin (@Param("login") String login);

@Query(value = " Select * From usuario Where senha Like CONCAT('%',:senha,'%') " ,nativeQuery = true)
List<Usuario> EncontrarPorLikeSenha (@Param("senha") String senha);

@Query(value = " Select * From usuario Where nome Like CONCAT('%',:nome,'%') " ,nativeQuery = true)
List<Usuario> EncontrarPorLikeNome (@Param("nome") String nome);


 
}
