package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface DocumentoRepository extends JpaRepository<Documento, Integer>{
 
@Query(value = " Select * From documento Where tipo = :tipo " ,nativeQuery = true)
List<Documento> EncontrarPorTipo (@Param("tipo") BigDecimal tipo);

@Query(value = " Select * From documento Where imagem = :imagem " ,nativeQuery = true)
List<Documento> EncontrarPorImagem (@Param("imagem") Byte[] imagem);

@Query(value = " Select * From documento Where observacao = :observacao " ,nativeQuery = true)
List<Documento> EncontrarPorObservacao (@Param("observacao") String observacao);

@Query(value = " Select d From Documento d ")
List<Documento> SelecionarTodosDocumento();

@Modifying
@Transactional
@Query(value = " Delete From Documento Where id_documento =:id_documento")
Integer DeletarPorId_documento (Integer id_documento);

@Modifying
@Transactional
@Query(value = " Delete From Documento Where tipo =:tipo")
Integer DeletarPorTipo (BigDecimal tipo);

@Modifying
@Transactional
@Query(value = " Delete From Documento Where imagem =:imagem")
Integer DeletarPorImagem (Byte[] imagem);

@Modifying
@Transactional
@Query(value = " Delete From Documento Where observacao =:observacao")
Integer DeletarPorObservacao (String observacao);

@Query(value = " Select d From Documento d Join Fetch d.id_usuario Where d.tipo =:tipo ")
Documento ProcurarPorDocumentoJoinFetchId_usuarioTipo(@Param("tipo") BigDecimal tipo);

@Query(value = " Select d From Documento d Join Fetch d.id_usuario Where d.imagem =:imagem ")
Documento ProcurarPorDocumentoJoinFetchId_usuarioImagem(@Param("imagem") Byte[] imagem);

@Query(value = " Select d From Documento d Join Fetch d.id_usuario Where d.observacao =:observacao ")
Documento ProcurarPorDocumentoJoinFetchId_usuarioObservacao(@Param("observacao") String observacao);

@Query(value = " Select d From Documento d Join Fetch d.id_certificado Where d.tipo =:tipo ")
Documento ProcurarPorDocumentoJoinFetchId_certificadoTipo(@Param("tipo") BigDecimal tipo);

@Query(value = " Select d From Documento d Join Fetch d.id_certificado Where d.imagem =:imagem ")
Documento ProcurarPorDocumentoJoinFetchId_certificadoImagem(@Param("imagem") Byte[] imagem);

@Query(value = " Select d From Documento d Join Fetch d.id_certificado Where d.observacao =:observacao ")
Documento ProcurarPorDocumentoJoinFetchId_certificadoObservacao(@Param("observacao") String observacao);

@Query(value = " Select * From documento Where tipo Like CONCAT('%',:tipo,'%') " ,nativeQuery = true)
List<Documento> EncontrarPorLikeTipo (@Param("tipo") BigDecimal tipo);

@Query(value = " Select * From documento Where imagem Like CONCAT('%',:imagem,'%') " ,nativeQuery = true)
List<Documento> EncontrarPorLikeImagem (@Param("imagem") Byte[] imagem);

@Query(value = " Select * From documento Where observacao Like CONCAT('%',:observacao,'%') " ,nativeQuery = true)
List<Documento> EncontrarPorLikeObservacao (@Param("observacao") String observacao);


 
}
