package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface ContatoRepository extends JpaRepository<Contato, Integer>{
 
@Query(value = " Select * From contato Where telefone_2 = :telefone_2 " ,nativeQuery = true)
List<Contato> EncontrarPorTelefone_2 (@Param("telefone_2") String telefone_2);

@Query(value = " Select * From contato Where email = :email " ,nativeQuery = true)
List<Contato> EncontrarPorEmail (@Param("email") String email);

@Query(value = " Select * From contato Where celular_2 = :celular_2 " ,nativeQuery = true)
List<Contato> EncontrarPorCelular_2 (@Param("celular_2") String celular_2);

@Query(value = " Select * From contato Where celular_1 = :celular_1 " ,nativeQuery = true)
List<Contato> EncontrarPorCelular_1 (@Param("celular_1") String celular_1);

@Query(value = " Select * From contato Where telefone_1 = :telefone_1 " ,nativeQuery = true)
List<Contato> EncontrarPorTelefone_1 (@Param("telefone_1") String telefone_1);

@Query(value = " Select c From Contato c ")
List<Contato> SelecionarTodosContato();

@Modifying
@Transactional
@Query(value = " Delete From Contato Where telefone_2 =:telefone_2")
Integer DeletarPorTelefone_2 (String telefone_2);

@Modifying
@Transactional
@Query(value = " Delete From Contato Where email =:email")
Integer DeletarPorEmail (String email);

@Modifying
@Transactional
@Query(value = " Delete From Contato Where celular_2 =:celular_2")
Integer DeletarPorCelular_2 (String celular_2);

@Modifying
@Transactional
@Query(value = " Delete From Contato Where celular_1 =:celular_1")
Integer DeletarPorCelular_1 (String celular_1);

@Modifying
@Transactional
@Query(value = " Delete From Contato Where telefone_1 =:telefone_1")
Integer DeletarPorTelefone_1 (String telefone_1);

@Modifying
@Transactional
@Query(value = " Delete From Contato Where id_contato =:id_contato")
Integer DeletarPorId_contato (Integer id_contato);

@Query(value = " Select c From Contato c Left join Fetch c.sobre Where c.telefone_2 =:telefone_2 ")
Contato ProcurarPorContatoLeftJoinFetchSobreTelefone_2(@Param("telefone_2") String telefone_2);

@Query(value = " Select c From Contato c Left join Fetch c.sobre Where c.email =:email ")
Contato ProcurarPorContatoLeftJoinFetchSobreEmail(@Param("email") String email);

@Query(value = " Select c From Contato c Left join Fetch c.sobre Where c.celular_2 =:celular_2 ")
Contato ProcurarPorContatoLeftJoinFetchSobreCelular_2(@Param("celular_2") String celular_2);

@Query(value = " Select c From Contato c Left join Fetch c.sobre Where c.celular_1 =:celular_1 ")
Contato ProcurarPorContatoLeftJoinFetchSobreCelular_1(@Param("celular_1") String celular_1);

@Query(value = " Select c From Contato c Left join Fetch c.sobre Where c.telefone_1 =:telefone_1 ")
Contato ProcurarPorContatoLeftJoinFetchSobreTelefone_1(@Param("telefone_1") String telefone_1);

@Query(value = " Select * From contato Where telefone_2 Like CONCAT('%',:telefone_2,'%') " ,nativeQuery = true)
List<Contato> EncontrarPorLikeTelefone_2 (@Param("telefone_2") String telefone_2);

@Query(value = " Select * From contato Where email Like CONCAT('%',:email,'%') " ,nativeQuery = true)
List<Contato> EncontrarPorLikeEmail (@Param("email") String email);

@Query(value = " Select * From contato Where celular_2 Like CONCAT('%',:celular_2,'%') " ,nativeQuery = true)
List<Contato> EncontrarPorLikeCelular_2 (@Param("celular_2") String celular_2);

@Query(value = " Select * From contato Where celular_1 Like CONCAT('%',:celular_1,'%') " ,nativeQuery = true)
List<Contato> EncontrarPorLikeCelular_1 (@Param("celular_1") String celular_1);

@Query(value = " Select * From contato Where telefone_1 Like CONCAT('%',:telefone_1,'%') " ,nativeQuery = true)
List<Contato> EncontrarPorLikeTelefone_1 (@Param("telefone_1") String telefone_1);


 
}
