package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface CertificadoRepository extends JpaRepository<Certificado, Integer>{
 
@Query(value = " Select * From certificado Where descricao = :descricao " ,nativeQuery = true)
List<Certificado> EncontrarPorDescricao (@Param("descricao") String descricao);

@Query(value = " Select c From Certificado c ")
List<Certificado> SelecionarTodosCertificado();

@Modifying
@Transactional
@Query(value = " Delete From Certificado Where descricao =:descricao")
Integer DeletarPorDescricao (String descricao);

@Modifying
@Transactional
@Query(value = " Delete From Certificado Where id_certificado =:id_certificado")
Integer DeletarPorId_certificado (Integer id_certificado);

@Query(value = " Select c From Certificado c Left join Fetch c.documento Where c.descricao =:descricao ")
Certificado ProcurarPorCertificadoLeftJoinFetchDocumentoDescricao(@Param("descricao") String descricao);

@Query(value = " Select * From certificado Where descricao Like CONCAT('%',:descricao,'%') " ,nativeQuery = true)
List<Certificado> EncontrarPorLikeDescricao (@Param("descricao") String descricao);


 
}
