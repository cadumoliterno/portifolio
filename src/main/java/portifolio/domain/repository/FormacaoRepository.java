package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface FormacaoRepository extends JpaRepository<Formacao, Integer>{
 
@Query(value = " Select * From formacao Where descricao = :descricao " ,nativeQuery = true)
List<Formacao> EncontrarPorDescricao (@Param("descricao") String descricao);

@Query(value = " Select f From Formacao f ")
List<Formacao> SelecionarTodosFormacao();

@Modifying
@Transactional
@Query(value = " Delete From Formacao Where id_formacao =:id_formacao")
Integer DeletarPorId_formacao (Integer id_formacao);

@Modifying
@Transactional
@Query(value = " Delete From Formacao Where descricao =:descricao")
Integer DeletarPorDescricao (String descricao);

@Query(value = " Select f From Formacao f Join Fetch f.id_usuario Where f.descricao =:descricao ")
Formacao ProcurarPorFormacaoJoinFetchId_usuarioDescricao(@Param("descricao") String descricao);

@Query(value = " Select * From formacao Where descricao Like CONCAT('%',:descricao,'%') " ,nativeQuery = true)
List<Formacao> EncontrarPorLikeDescricao (@Param("descricao") String descricao);


 
}
