package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer>{
 
@Query(value = " Select * From endereco Where uf = :uf " ,nativeQuery = true)
List<Endereco> EncontrarPorUf (@Param("uf") String uf);

@Query(value = " Select * From endereco Where bairro = :bairro " ,nativeQuery = true)
List<Endereco> EncontrarPorBairro (@Param("bairro") String bairro);

@Query(value = " Select * From endereco Where logradouro = :logradouro " ,nativeQuery = true)
List<Endereco> EncontrarPorLogradouro (@Param("logradouro") String logradouro);

@Query(value = " Select * From endereco Where cidade = :cidade " ,nativeQuery = true)
List<Endereco> EncontrarPorCidade (@Param("cidade") String cidade);

@Query(value = " Select * From endereco Where complemento = :complemento " ,nativeQuery = true)
List<Endereco> EncontrarPorComplemento (@Param("complemento") String complemento);

@Query(value = " Select * From endereco Where rua = :rua " ,nativeQuery = true)
List<Endereco> EncontrarPorRua (@Param("rua") String rua);

@Query(value = " Select e From Endereco e ")
List<Endereco> SelecionarTodosEndereco();

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where uf =:uf")
Integer DeletarPorUf (String uf);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where bairro =:bairro")
Integer DeletarPorBairro (String bairro);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where logradouro =:logradouro")
Integer DeletarPorLogradouro (String logradouro);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where cidade =:cidade")
Integer DeletarPorCidade (String cidade);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where complemento =:complemento")
Integer DeletarPorComplemento (String complemento);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where rua =:rua")
Integer DeletarPorRua (String rua);

@Modifying
@Transactional
@Query(value = " Delete From Endereco Where id_endereco =:id_endereco")
Integer DeletarPorId_endereco (Integer id_endereco);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.uf =:uf ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreUf(@Param("uf") String uf);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.bairro =:bairro ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreBairro(@Param("bairro") String bairro);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.logradouro =:logradouro ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreLogradouro(@Param("logradouro") String logradouro);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.cidade =:cidade ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreCidade(@Param("cidade") String cidade);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.complemento =:complemento ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreComplemento(@Param("complemento") String complemento);

@Query(value = " Select e From Endereco e Left join Fetch e.sobre Where e.rua =:rua ")
Endereco ProcurarPorEnderecoLeftJoinFetchSobreRua(@Param("rua") String rua);

@Query(value = " Select * From endereco Where uf Like CONCAT('%',:uf,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeUf (@Param("uf") String uf);

@Query(value = " Select * From endereco Where bairro Like CONCAT('%',:bairro,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeBairro (@Param("bairro") String bairro);

@Query(value = " Select * From endereco Where logradouro Like CONCAT('%',:logradouro,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeLogradouro (@Param("logradouro") String logradouro);

@Query(value = " Select * From endereco Where cidade Like CONCAT('%',:cidade,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeCidade (@Param("cidade") String cidade);

@Query(value = " Select * From endereco Where complemento Like CONCAT('%',:complemento,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeComplemento (@Param("complemento") String complemento);

@Query(value = " Select * From endereco Where rua Like CONCAT('%',:rua,'%') " ,nativeQuery = true)
List<Endereco> EncontrarPorLikeRua (@Param("rua") String rua);


 
}
