package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface HabilidadeRepository extends JpaRepository<Habilidade, Integer>{
 
@Query(value = " Select * From habilidade Where descricao = :descricao " ,nativeQuery = true)
List<Habilidade> EncontrarPorDescricao (@Param("descricao") String descricao);

@Query(value = " Select h From Habilidade h ")
List<Habilidade> SelecionarTodosHabilidade();

@Modifying
@Transactional
@Query(value = " Delete From Habilidade Where descricao =:descricao")
Integer DeletarPorDescricao (String descricao);

@Modifying
@Transactional
@Query(value = " Delete From Habilidade Where id_habilidade =:id_habilidade")
Integer DeletarPorId_habilidade (Integer id_habilidade);

@Query(value = " Select h From Habilidade h Join Fetch h.id_usuario Where h.descricao =:descricao ")
Habilidade ProcurarPorHabilidadeJoinFetchId_usuarioDescricao(@Param("descricao") String descricao);

@Query(value = " Select * From habilidade Where descricao Like CONCAT('%',:descricao,'%') " ,nativeQuery = true)
List<Habilidade> EncontrarPorLikeDescricao (@Param("descricao") String descricao);


 
}
