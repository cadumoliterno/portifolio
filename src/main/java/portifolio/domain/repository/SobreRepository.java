package portifolio.domain.repository;
import java.util.List;
import java.util.Calendar;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import portifolio.domain.entity.*;

public interface SobreRepository extends JpaRepository<Sobre, Integer>{
 
@Query(value = " Select * From sobre Where descricao = :descricao " ,nativeQuery = true)
List<Sobre> EncontrarPorDescricao (@Param("descricao") String descricao);

@Query(value = " Select s From Sobre s ")
List<Sobre> SelecionarTodosSobre();

@Modifying
@Transactional
@Query(value = " Delete From Sobre Where descricao =:descricao")
Integer DeletarPorDescricao (String descricao);

@Modifying
@Transactional
@Query(value = " Delete From Sobre Where id_sobre =:id_sobre")
Integer DeletarPorId_sobre (Integer id_sobre);

@Query(value = " Select s From Sobre s Join Fetch s.id_usuario Where s.descricao =:descricao ")
Sobre ProcurarPorSobreJoinFetchId_usuarioDescricao(@Param("descricao") String descricao);

@Query(value = " Select s From Sobre s Join Fetch s.id_contato Where s.descricao =:descricao ")
Sobre ProcurarPorSobreJoinFetchId_contatoDescricao(@Param("descricao") String descricao);

@Query(value = " Select s From Sobre s Join Fetch s.id_endereco Where s.descricao =:descricao ")
Sobre ProcurarPorSobreJoinFetchId_enderecoDescricao(@Param("descricao") String descricao);

@Query(value = " Select * From sobre Where descricao Like CONCAT('%',:descricao,'%') " ,nativeQuery = true)
List<Sobre> EncontrarPorLikeDescricao (@Param("descricao") String descricao);


 
}
